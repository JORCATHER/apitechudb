package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll() {

        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public PurchaseModel save(PurchaseModel purchase) {
        System.out.println("save en PurchaseRepository");

        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseRepository");

        Optional<PurchaseModel> result = Optional.empty();

        //Búsqueda de la compra por id
        for (PurchaseModel purchase : ApitechudbApplication.purchaseModels) {
            if (purchase.getId().equals(id)) {
                System.out.println("Compra encontrada, id: " + id);
                result = Optional.of(purchase);
            }
        }

        return result;
    }

}
