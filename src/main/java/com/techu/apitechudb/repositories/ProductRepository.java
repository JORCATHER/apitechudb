package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll() {

        System.out.println("findAll en ProductRepository");

        return ApitechudbApplication.productModels;
    }

    public ProductModel save(ProductModel product) {
        System.out.println("save en ProductRepository");

        ApitechudbApplication.productModels.add(product);

        return product;
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductRepository");

        Optional<ProductModel> result = Optional.empty();

        //Búsqueda del producto por id
        for (ProductModel product : ApitechudbApplication.productModels) {
            if (product.getId().equals(id)) {
               System.out.println("Producto encontrado, id: " + id);
               result = Optional.of(product);
            }
        }

        return result;
    }

    public Optional<ProductModel> update(ProductModel product, String id) {
        System.out.println("update en ProductRepository");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("El producto a actualizar es: " + product);
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        Optional<ProductModel> result = this.findById(id);

        if (result.isPresent()) {
            result.get().setId(product.getId());
            result.get().setDesc(product.getDesc());
            result.get().setPrice(product.getPrice());
            System.out.println("producto actualizado en ProductRepository: " + product);
        }

        return result;
    }

    public Optional<ProductModel> delete(String id) {
        System.out.println("delete en ProductRepository");
        System.out.println("La id del producto a borrar es " + id);

        Optional<ProductModel> result = this.findById(id);

        if (result.isPresent()) {
            System.out.println("producto borrado en ProductRepository: " + result.get());
            ApitechudbApplication.productModels.remove(result.get());
        }

        return result;
    }

    public void delete2(ProductModel product) {
        System.out.println("delete2 en ProductRepository");
        System.out.println("El producto a borrar es " + product);

        ApitechudbApplication.productModels.remove(product);
    }

}
