package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    public List<UserModel> findAll() {

        System.out.println("findAll en UserRepository");

        return ApitechudbApplication.userModels;
    }

    public UserModel save(UserModel user) {
        System.out.println("save en UserRepository");

        ApitechudbApplication.userModels.add(user);

        return user;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserRepository");

        Optional<UserModel> result = Optional.empty();

        //Búsqueda de usuario por id
        for (UserModel user : ApitechudbApplication.userModels) {
            if (user.getId().equals(id)) {
                System.out.println("Usuario encontrado, id: " + id);
                result = Optional.of(user);
            }
        }

        return result;
    }

    public Optional<UserModel> update(UserModel user, String id) {
        System.out.println("update en UserRepository");
        System.out.println("La id del usuario a actualizar es " + id);
        System.out.println("El usuario a actualizar es: " + user);
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        Optional<UserModel> result = this.findById(id);

        if (result.isPresent()) {
            result.get().setId(user.getId());
            result.get().setName(user.getName());
            result.get().setAge(user.getAge());
            System.out.println("usuario actualizado en userRepository: " + user);
        }

        return result;
    }

    public Optional<UserModel> delete(String id) {
        System.out.println("delete en UserRepository");
        System.out.println("La id del usuario a borrar es " + id);

        Optional<UserModel> result = this.findById(id);

        if (result.isPresent()) {
            System.out.println("usuario borrado en UserRepository: " + result.get());
            ApitechudbApplication.userModels.remove(result.get());
        }

        return result;
    }

    public void delete2(UserModel user) {
        System.out.println("delete2 en UserRepository");
        System.out.println("El usuario a borrar es " + user);

        ApitechudbApplication.userModels.remove(user);
    }

}
