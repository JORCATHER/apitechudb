package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getTestDataUsers();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestDataPurchases();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						, "Producto 1"
						, 10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						, "Producto 2"
						, 20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						, "Producto 3"
						, 30
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getTestDataUsers() {
		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1",
						"Jorge",
						35
				)
		);

		userModels.add(
				new UserModel(
						"2",
						"Paco",
						99
				)
		);

		userModels.add(
				new UserModel(
						"3",
						"Felipe",
						13
				)
		);

		return userModels;
	}
	private static ArrayList<PurchaseModel> getTestDataPurchases() {
		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();

		Map<String, Integer> purchaseItems = new HashMap<>();
		purchaseItems.put("1", 23);
		purchaseItems.put("2", 35);

		purchaseModels.add(
				new PurchaseModel(
						"1",
						"1",
						1200,
						purchaseItems
				)
		);

		return purchaseModels;
	}
}


