package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/users")
public class UserController {

    @Autowired
    UserService userService;

    // Todos los usuarios
    //Obtener todos los usuarios con una edad concreta

    @GetMapping()
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "age", defaultValue = "SN") String age) {
        System.out.println("getUsers en UserController");
        System.out.println("parámetro opcional age: " + age);

        return new ResponseEntity<>(
                this.userService.findAll(age),
                HttpStatus.OK
        );
    }

    //Obtener un usuario en base a su id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById en UserController");
        System.out.println("La id del usuario a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Crear un usuario
    @PostMapping()
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser en UserController");
        System.out.println("El usuario a crear es: " + user);


        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    //Actualizar completamente un usuario en base a su id
    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser en UserController");
        System.out.println("La id del usuario a actualizar es: " + id);
        System.out.println("El usuario a actualizar es: " + user);

        Optional<UserModel> result = this.userService.updateUser(user, id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Borrar un usuario en base a su id
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser en UserController");
        System.out.println("La id del usuario a borrar es: " + id);

        Optional<UserModel> result = this.userService.delete(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
