package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/apitechu/v2/purchases")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping()
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases en PurchasesController");

        return new ResponseEntity<>(
                this.purchaseService.findAll(),
                HttpStatus.OK
        );
    }

    //Crear una compra
    @PostMapping()
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase en PurchaseController");
        System.out.println("La compra a crear es: " + purchase);

        Map<String, PurchaseModel> result = this.purchaseService.add(purchase);
// Otro tipo de retorno para controlar los errores que devolvemos
        return new ResponseEntity<>(
                result.containsKey("OK") ? result.get("OK") : result.keySet().toArray()[0],
                result.containsKey("OK") ? HttpStatus.OK : HttpStatus.BAD_REQUEST
        );
    }

}
