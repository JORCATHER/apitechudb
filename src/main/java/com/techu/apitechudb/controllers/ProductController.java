package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("El producto a crear es: " + product);


        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es: " + id);
        System.out.println("El producto a actualizar es: " + product);

        Optional<ProductModel> result = this.productService.updateProduct(product, id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es: " + id);

        Optional<ProductModel> result = this.productService.delete(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }




}
