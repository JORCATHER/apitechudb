package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseService");

        List<PurchaseModel> result = this.purchaseRepository.findAll();

        return result;
    }

    public Map<String, PurchaseModel> add(PurchaseModel purchase) {
        System.out.println("add en PurchaseService");

        Map<String, PurchaseModel> result = new HashMap<>();

        String validations = this.verifyPurchase(purchase);

        if (validations.equals("OK")) {
            float amount = calcAmount(purchase);
            System.out.println("Compra correcta, calculando amount:" + amount);
            purchase.setAmount(amount);

            System.out.println("Compra correcta, guardamos compra: " + purchase);
            result.put(validations, this.purchaseRepository.save(purchase));
        } else {
            result.put(validations, null);
        }

        return result;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    private String verifyPurchase(PurchaseModel purchase) {
        System.out.println("Verificando compra en PurchaseService");

        String result = "OK";

        Optional<UserModel> user = this.userService.findById(purchase.getUserId());

        // Comprobar que el usuario existe
        if (!user.isPresent()) {
                result = "El usuario indicado no existe";
                System.out.println("El usuario en PurchaseService no existe");
        }

        // Comprobar que todos los productos de la cesta existen
        if (result.equals("OK")) {
            for (String id : purchase.getPurchaseItems().keySet()) {
                if (!this.productService.findById(id).isPresent()){
                    result = "El producto " + id + " de la cesta no existe en el inventario";
                    System.out.println("El producto no exsite: " + id);
                }
            }
        }

        //Comprobar que no haya una compra con esa id.
        if (result.equals("OK")) {
            if (this.findById(purchase.getId()).isPresent()) {
                result = "La compra " + purchase.getId() + " ya existe";
                System.out.println("La compra " + purchase.getId() + " ya existe");
            }
        }

        System.out.println("Resultado verificación: " + result);
        return result;
    }

    private float calcAmount(PurchaseModel purchase) {

        System.out.println("Calculando amount de la purchase");
        float amount = 0;

        for (String id : purchase.getPurchaseItems().keySet()) {

            Optional<ProductModel> result = this.productService.findById(id);

            if (result.isPresent()) {
                System.out.println("Añadido producto: " + id + " amount: " + amount);
                amount += (purchase.getPurchaseItems().get(id) * result.get().getPrice());
            } else {
                System.out.println("Error, producto no encontrado: " + id);
            }
        }

        return amount;
    }
}
