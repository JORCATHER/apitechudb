package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String age) {
        System.out.println("findAll en UserService");

        List<UserModel> result = this.userRepository.findAll();;

        if (age.chars().allMatch(Character::isDigit)) {
            int age2 = Integer.parseInt(age);
            result = result.stream().filter(user -> user.getAge() == age2).collect(Collectors.toList());
        }

        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public Optional<UserModel> updateUser(UserModel user, String id) {
        System.out.println("updateUser en UserService");

        return this.userRepository.update(user, id);
    }

    public Optional<UserModel> delete(String id) {
        System.out.println("delete en UserService");

        return this.userRepository.delete(id);
    }

}
